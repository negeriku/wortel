use handlebars;
use serde_json::json;
use std::fs::File;
use std::env;
use std::io::Read;
use std::process;

/// Datangkan Struct Handlebars dari module handlerbars
/// Apakah dia akan ikut dilibatkan, atau hanya dipanggil ?
use handlebars::Handlebars;

fn main() {
    // new();
    get_controllers();
}

fn get_controllers() {
    println!("{:?}", env::current_dir().expect("Error"));
    let routes_filename = "/home/insan/Research/rust-rapid-restful-api-dev/embedded_resources/warp-based-restful-api/src/routes.rs";
    let mut file = File::open(&routes_filename).expect("Unable to open file");

    let mut src = String::new();
    file.read_to_string(&mut src).expect("Unable to read file");

    let syntax = syn::parse_file(&src).expect("Unable to parse file");

    println!("{:#?}", syntax);
}

fn new() {
    println!("Create new project");

    create_cargo_toml_file();
    copy_boilerplate_files();
}

fn create_cargo_toml_file() {
    // Get Cargo.toml bytes to executable 
    let cargo_toml_bytes = include_bytes!("../embedded_resources/warp-based-restful-api/Cargo.toml");

    // Get Cargo.toml bytes as String template
    let cargo_toml_str_tpl : String = String::from_utf8_lossy(cargo_toml_bytes).into_owned();

    // Create a new Handlebars instance
    let hb = Handlebars::new();

    // Add variables to Cargo.toml template
    let cargo_toml_str : String = match hb.render_template(&cargo_toml_str_tpl, &json!({
        "package_name": "dolban",
        "author_name": "anggita",
        "author_email": "fzhanggita@gmail.com",
    })) {
        Ok(result) => result, 
        Err(_) => "".to_string(), 
    };

    // fs merupakan library terkait file system
    // Hasil dari fungsi write() merupakan Result
    // .expect() memberikan penanganan terhadap hasil result
    std::fs::write("/home/insan/Research/woro-woro/Cargo.toml", cargo_toml_str).expect("Unable write to file");
}

fn copy_boilerplate_files() {
    std::fs::create_dir_all("/home/insan/Research/woro-woro/src").expect("Unable to create directory"); 
    std::fs::create_dir_all("/home/insan/Research/woro-woro/src/modules/basic/handlers").expect("Unable to create directory"); 
    std::fs::create_dir_all("/home/insan/Research/woro-woro/src/modules/basic/models").expect("Unable to create directory"); 
}
