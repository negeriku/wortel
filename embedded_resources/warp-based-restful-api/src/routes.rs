use warp::{filters::BoxedFilter, Filter, Reply};

use crate::modules::basic;
use crate::modules::basic::utils::dbconnection::with_db;
use crate::modules::basic::utils::pdf::get_pdf_headers;

pub fn apply() -> BoxedFilter<(impl Reply,)> {
    let db_pool = basic::utils::dbconnection::get_db_pool();

    let healthcheck = warp::path!("healthcheck").map(|| {
        println!("Perform healthcheck");
        format!("hehehe")
    });

    let login = warp::path!("login")
        .and(warp::post())
        .and(with_db(db_pool.clone()))
        .and(warp::body::json())
        .and_then(basic::handlers::login);

    login
        .or(healthcheck)
        .recover(basic::handlers::handle_rejection)
        .boxed()
}
