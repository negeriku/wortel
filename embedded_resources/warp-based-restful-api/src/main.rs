mod modules;
mod routes;

async fn create_assets_folder() -> std::io::Result<()> {
    println!("create dir");
    std::fs::create_dir_all("/assets/pendataan")?;

    let paths = std::fs::read_dir("/").unwrap();
    for path in paths {
        println!("Name: {}", path.unwrap().path().display())
    }
    Ok(())
}

#[tokio::main]
async fn main() {
    match create_assets_folder().await {
        Ok(_) => {
            println!("berhasil buat dir baru")
        }
        Err(e) => {
            println!("gagal buat dir baru karena {:?}", e)
        }
    };

    let routes = routes::apply();

    println!("Server started...");
    warp::serve(routes).run(([0, 0, 0, 0], 3030)).await;
}
